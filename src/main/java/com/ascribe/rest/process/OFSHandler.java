package com.ascribe.rest.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.ascribe.rest.models.Command;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class OFSHandler {
    private static final Logger log = Logger.getLogger(OFSHandler.class);

    public HashMap<String, Object> runCmd(List<Command> cmd) {
        HashMap<String, Object> result = new HashMap<>();
        log.info("");
        log.info("Command: " + cmd);
        int exitCode = 0;
        try {

            cmd.stream().forEachOrdered(i -> {
                String workingDir = i.getWorkingDir();

                List<String> list = new ArrayList<String>();
                Arrays.asList(i.getCommand().split(" ")).stream().forEach(j -> list.add(j));

                ProcessBuilder build = new ProcessBuilder(list);
                build.redirectErrorStream(true);

                if (build.redirectErrorStream()) {
                    log.info("Redirected error stream to output stream");
                }

                if (!workingDir.isEmpty()) {
                    build.directory(new File(workingDir));
                    log.info("Changed working directory to: " + workingDir);
                }

                Process process = null;
                String s = null;

                try {
                    process = build.start();
                    BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));

                    while ((s = br.readLine()) != null) {
                        break;
                    }
                    log.info("Output: " + s);
                    log.info("");

                } catch (IOException e) {
                    log.error(e);
                } finally {
                    try {
                        process.destroy();
                    } catch (Exception e) {
                        log.error(e);
                    }
                }

                result.put("status", HttpStatus.OK);
                result.put("description", s);

            });

        } catch (Exception e) {
            log.error(e);
        }
        return result;
    }
}