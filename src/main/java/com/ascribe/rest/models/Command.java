package com.ascribe.rest.models;

public class Command {

    private String command;
    private String workingDir;

    public Command() {
    }

    public Command(String command, String workingDir) {
        this.command = command;
        this.workingDir = workingDir;
    }

    public String getCommand() {
        return this.command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getWorkingDir() {
        return this.workingDir;
    }

    public void setWorkingDir(String workingDir) {
        this.workingDir = workingDir;
    }

    public Command command(String command) {
        this.command = command;
        return this;
    }

    public Command workingDir(String workingDir) {
        this.workingDir = workingDir;
        return this;
    }

    @Override
    public String toString() {
        return "{" + " command='" + getCommand() + "'" + ", workingDir='" + getWorkingDir() + "'" + "}";
    }
}