package com.ascribe.rest.controllers;

import java.util.HashMap;
import java.util.List;

import com.ascribe.rest.models.Command;
import com.ascribe.rest.process.OFSHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OFSController {

    @Autowired
    private OFSHandler oh;

    @GetMapping("/")
    public ResponseEntity<HashMap<String, String>> greeting() {
        HashMap<String, String> msg = new HashMap<>();
        msg.put("description", "hello world");
        return ResponseEntity.status(HttpStatus.OK).body(msg);
    }

    @PostMapping("/run")
    public ResponseEntity<Object> runCommand(@RequestBody List<Command> cmd) {
        HashMap<String, Object> result = oh.runCmd(cmd);
        return ResponseEntity.status((HttpStatus) result.get("status")).body(result);
    }
}